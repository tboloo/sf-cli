# sfdx-cli ARM64 docker image

This is instant, ready to use salesforce development environment to work on ARM64 Mac using Visual Studio Code [remote-containers](https://code.visualstudio.com/docs/devcontainers/containers)

## Getting started

When using this solution make sure that you have `ssh-agent` running on your host, and that the repository key has been added to it via

```
➜ ssh-add path_to_private_key
```

verify that `ssh-add -l` on both host and docker shows the same key.

Start with connecting your org:

```
[main ≡]> cci org connect your-arg-alias
```

and make sure that it worked

```
[main ≡]> cci org list
```

## References

[CumulusCI documentation](https://cumulusci.readthedocs.io/en/stable/intro.html)
